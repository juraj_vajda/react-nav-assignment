import React, { Component } from 'react';
import TabNav from './components/navigation.component';

class App extends Component {
	render() {
		return (
			<header className="header">
				<TabNav />
			</header>
		);
	}
}

export default App;
