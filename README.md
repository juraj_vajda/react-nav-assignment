This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to run this App

In the project directory, you can run:

### `npm install`

Will install all node js dependencies.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
